﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodingExercise
{
    class Arrangement
    {
        public Arrangement()
        {

        }
        public Dictionary<string, int> aboveBelow(List<int> numCollection, int compValue)
        {

            //method variables
            Dictionary<string, int> orderValue = new Dictionary<string, int>();
            int a = 0;//above 
            int b = 0;//below

            //checks if number in list is above or below given will not add to above or below if the number is the same.
            foreach (int num in numCollection)
            {
                if (num > compValue)
                {
                    a++;
                }
                else if (num < compValue)
                {
                    b++;
                }
            }

            orderValue.Add("Above", a);
            orderValue.Add("Below", b);


            return orderValue;
        }
        public String stringRoation(string phrase, int rotation)
        {
            //method variables
            String phraseRotate = phrase;
            int overflow = phrase.Length - (rotation % phrase.Length);

            //rotation
            phraseRotate = phraseRotate.Substring(overflow) + phraseRotate.Substring(0,overflow);

            return phraseRotate;
        }
    }
}
